const playlist = [
  {
    title: 'Skyfall',
    album: 'Skyfall Original Soundtrack',
    artist: 'Adele',
    cover: 'skyfall.jpg',
    src: 'skyfall.m4a',
    duration: 289,
  },
  {
    title: 'Decisive Battle',
    album: 'Shin Godzilla Original Soundtrack',
    artist: 'Shirō Sagisu',
    cover: 'decisive-battle.jpg',
    src: 'decisive-battle.m4a',
    duration: 145,
  },
  {
    title: 'Beauty Song',
    album: 'House of Flying Daggers Original Soundtrack',
    artist: 'Zhang Ziyi',
    cover: 'beauty-song.jpg',
    src: 'beauty-song.m4a',
    duration: 154,
  },
  {
    title: 'drivers license',
    album: 'sour',
    artist: 'Olivia Rodrigo',
    cover: 'drivers-license.jpg',
    src: 'drivers-license.m4a',
    duration: 247,
  },
  {
    title: 'POP/STARS',
    album: 'POP/STARS',
    artist: 'K/DA',
    cover: 'kda.jpg',
    src: 'pop-stars.m4a',
    duration: 202,
  },
  {
    title: 'Somebody That I Used To Know',
    album: 'Making Mirrors',
    artist: 'Gotye',
    cover: 'making-mirrors.jpg',
    src: 'somebody-that-i-used-to-know.m4a',
    duration: 243,
  },
]

export default playlist

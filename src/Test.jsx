import playlist from 'playlist'
import { useRef, useState } from 'react'
import c from './media-player.module.scss'

function App() {
  // audio
  const [selectedSong, setSelectedSong] = useState(null)
  const track = useRef()

  // playback
  const play = () => {
    track.current.play()
  }

  const pause = () => {
    track.current.pause()
  }

  // metadata
  const [currentTime, setCurrentTime] = useState(0)
  const [duration, setDuration] = useState(0)

  // playlist
  const selectSong = (song) => {
    console.log(song)
    setSelectedSong(song)
  }

  const list = playlist.map((item, i) => {
    return (
      <li key={i} onClick={() => selectSong(item)}>
        <div className={c.albumThumb}>
          <img src={`/covers/${item.cover}`} alt={item.title} />
        </div>
        <div className={c.meta}>
          <span className={c.title}>{item.title}</span>
          <span className={c.title}>{item.album}</span>
          <span className={c.title}>{item.artist}</span>
        </div>
        <div className={c.duration}>
          <span>
            {Math.floor(item.duration / 60)}: {item.duration % 60}
          </span>
        </div>
      </li>
    )
  })

  // render

  return (
    <div className='App'>
      {selectSong?.src}
      <audio
        src={`/audio/${selectedSong?.src}`}
        ref={track}
        onLoadedData={() => setDuration(track.current.duration)}
        onTimeUpdate={() => setCurrentTime(track.current.currentTime)}
      />
      <h2>
        {currentTime} / {duration}
      </h2>
      <button onClick={play}>Play</button>
      <button onClick={pause}>Pause</button>
      <ul className={c.playlist}>{list}</ul>
    </div>
  )
}

export default App

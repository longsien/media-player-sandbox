import playlist from 'playlist'
import { useEffect, useRef, useState } from 'react'
import c from './media-player.module.scss'

function App() {
  // audio
  const [track, setTrack] = useState(null)
  const audio = useRef()

  // playback
  const play = () => {
    audio.current.volume = 0.1
    audio.current.play()
  }

  const pause = () => {
    audio.current.pause()
  }

  // meta
  const [duration, setDuration] = useState(0)
  const [currentTime, setCurrentTime] = useState(0)

  const [handlePostion, setHandlePostion] = useState(0)

  useEffect(() => {
    setHandlePostion((audio.current.currentTime / audio.current.duration) * 100)
  }, [currentTime, duration])

  // playlist
  const list = playlist.map((item, i) => {
    return (
      <li className={c.playlistItem} key={i} onClick={() => setTrack(item)}>
        <div className={c.cover}>
          <img src={`/covers/${item.cover}`} alt={item.title} />
        </div>
        <div className={c.meta}>
          <div className={c.title}>{item.title}</div>
          <div className={c.artist}>{item.artist}</div>
        </div>
        <div className={c.duration}>
          {Math.floor(item.duration / 60)}:{item.duration % 60}
        </div>
      </li>
    )
  })

  // render

  const dragAndDrop = (e) => {
    let width = e.target.parentElement.getBoundingClientRect().width
    let drag = e.clientX

    const doSomething = (e) => {
      let diff = e.clientX - drag
      let moved = (diff/width)
      let seek = audio.current.duration * moved
      audio.current.currentTime = audio.current.currentTime + seek

      document.removeEventListener('mouseup', doSomething)
    }

    document.addEventListener('mouseup', doSomething)
    
  }

  return (
    <div className='App'>
      {/* audio */}
      <audio
        src={`/audio/${track?.src}`}
        ref={audio}
        onLoadedData={() =>
          setDuration(
            `${Math.floor(audio.current.duration / 60)}:${
              Math.floor(audio.current.duration) % 60
            }`
          )
        }
        onTimeUpdate={() =>
          setCurrentTime(
            `${Math.floor(audio.current.currentTime / 60)}:${
              Math.floor(audio.current.currentTime) % 60
            }`
          )
        }
      />

      {/* player */}
      <div className={c.player}>
        <div className={c.cover}>
          <img src={`/covers/${track?.cover}`} alt={track?.title} />
        </div>
        <h1>{track?.title}</h1>
        <div className={c.playbar}>
          <div className={c.currentTime}>{currentTime}</div>
          <div className={c.bar}>
            <div 
              className={c.handle} 
              style={{ left: `${handlePostion}%` }} 
              // onMouseDown={(e) => console.log(e)}
              onMouseDown={(e) => {
                e.preventDefault()
                console.log(e.clientX)
                dragAndDrop(e)
              }}
              
            />
          </div>
          <div className={c.duration}>{duration}</div>
        </div>
        <button onClick={play}>Play</button>
        <button onClick={pause}>Pause</button>
      </div>

      {/* playist */}
      <ul className={c.playlist}>{list}</ul>
    </div>
  )
}

export default App
